package com.tecsup.lab.calif01.view;

import com.tecsup.lab.calif01.controller.UserController;

public class UserView {

	public static void imprimir() {
		UserController u = new UserController();
		
		//imprimir datos
		System.out.println(">> Datos del usuario:");
		System.out.println("Nombres: " + u.obtener_nombre());
		System.out.println("Apellidos: " + u.obtener_apellido());
		System.out.println("Edad: " + u.obtener_edad());
	}
	
}
